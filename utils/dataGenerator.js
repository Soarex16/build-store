function randInt(min, max) {
  let rand = min - 0.5 + Math.random() * (max - min + 1);
  rand = Math.round(rand);
  
  return rand;
}

function randStr(len) {
  let result = "";
  
  for(let i = 0; i < Math.trunc(len / 10); ++i) {
    result += Math.random().toString(36).substring(2, 12);
  }
  
  return result + Math.random().toString(36).substring(2, 3 + len - len / 10);
} 

const builds = [];

const buildNum = 37;

const platforms = ['windows', 'android', 'ios'];

for(let i = 0; i < buildNum; ++i) {
  let carouselInfo = {
    images: Array(randInt(1, 12), ' ').map(() => 'https://source.unsplash.com/random')
  };
  
  let buildId = i;//randInt(100000, 999999)
  
  let buildInfo = {
    id: buildId,
    targetPlatform: platforms[randInt(0, 2)],
    version: i,//randStr(12),
    buildDate: new Date(randInt(0, 100000000)),
    bugsNum: randInt(0, 150),
    fileSize: Math.round(Math.random() * 10, 2) + "Mb",
    downloadLink: "https://www.example.ru/builds/" + randStr(12)
  };
  
  let productInfo = {
    title: "Lorem ipsum dolor",
    description: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat."
  };
  
  builds.push({
    id: buildId,
    build: buildInfo,
    product: productInfo,
    carousel: carouselInfo
  });
}

let str = JSON.stringify(builds);

let fs = require('fs');
fs.writeFile("testData.json", str, (err) => {
  if (err) console.log(err);
  console.log("Successfully Written to File.");
});