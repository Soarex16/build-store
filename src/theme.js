import {createMuiTheme} from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        primary: {
            light: '#80e27e',
            main: '#4caf50',
            dark: '#087f23',
            contrastText: '#ffffff'
        }, secondary: {
            light: '#fff350',
            main: '#ffc107',
            dark: '#c79100',
            contrastText: '#ffffff'
        },
        text: {
            primary: "#212121",
            secondary: "#757575",
            disabled: "#9e9e9e",
            hint: "#9e9e9e"
        }
    }
});

export const textCaption = {
    color: theme.palette.text.hint,
    textTransform: "uppercase",
    letterSpacing: "0.15em",
};

export default theme;