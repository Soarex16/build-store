import React from 'react';

//m-ui theme
import appTheme from './theme'
import {ThemeProvider} from '@material-ui/styles';

import CssBaseline from '@material-ui/core/CssBaseline';

//custom components
import Header from './components/header/Header';
import Content from './components/body/Content';
import Footer from './components/footer/Footer';

/**
 * Note: in the future it is worth thinking about the layout more friendly to the reusability
 * of components, in accordance with the article: https://habr.com/ru/en/post/326484/
 *
 * The application is divided into components in accordance with the semantic layout introduced in HTML5
 * Header: contains hero panel and main menu
 * Content: main part of application, that holds all business logic
 * Footer: social buttons and copyright
 */

class App extends React.Component {
    render() {
        return (
            <React.Fragment>
                <CssBaseline />
                <ThemeProvider theme={appTheme}>
                    <Header />
                    <Content />
                    <Footer />
                </ThemeProvider>
            </React.Fragment>
        );
    }
}

export default App;
