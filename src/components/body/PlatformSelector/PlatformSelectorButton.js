import React from 'react';

import {withStyles} from '@material-ui/styles';

import {ButtonBase, SvgIcon} from '@material-ui/core';

const styles = theme => ({
    button: {
        fontFamily: "Roboto",
        fontWeight: theme.typography.fontWeightRegular,
        borderRadius: 6,
        fontSize: "0.975rem",

        letterSpacing: "0.02870em",
        textTransform: "uppercase",
        flexDirection: "column",

        lineHeight: 2.25,
        color: theme.palette.grey[400],

        width: "100%",
        height: theme.spacing(12),

        border: "solid 0.07rem rgb(192, 192, 192)",
        background: theme.palette.common.white,

        transition: theme.transitions.create(
            ['background', 'color'],
            { duration: theme.transitions.duration.complex}
        )
    },

    buttonToggled: props => ({
        background: 'linear-gradient(45deg, ' + props.gradient.left + ' 30%, ' + props.gradient.right + ' 90%)',
        color: 'white',
    })
});

function PlatformSelectorButton(props) {
    const handleClick = () => {
        props.onToggle(props.os);
    };

    const {classes} = props;

    const selectedStyle = props.toggled ? classes.buttonToggled : "";

    return (
        <ButtonBase
            onClick={handleClick}
            className={[classes.button, selectedStyle].join(' ')}
        >
            {props.os}
            <SvgIcon fontSize={"large"}>
                {props.children}
            </SvgIcon>
        </ButtonBase>
    );
}

export default withStyles(styles)(PlatformSelectorButton);