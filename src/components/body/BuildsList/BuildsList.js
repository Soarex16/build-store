import React from 'react';

import {withStyles} from '@material-ui/styles';
import {Grid, LinearProgress} from '@material-ui/core';

import InfiniteScroll from 'react-infinite-scroll-component';

import BuildCard from './BuildCard';
import BuildsListHeader from './BuildsListHeader'

const styles = theme => ({
    scroll : {
        paddingLeft: theme.spacing(3),
        paddingRight: theme.spacing(3),

        paddingBottom: theme.spacing(3),
    },

    listHeader: {
        paddingLeft: theme.spacing(3),
        paddingRight: theme.spacing(3)
    },

    card: {
        marginTop: theme.spacing(3)
    },

    progress: {
        marginTop: theme.spacing(3),
        borderRadius: 4
    }
});

// only for stub
const _buildsNum = 5;
const _fetchDelay = 500;

class BuildsList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            _lastItem: _buildsNum,
            cardsOnPage: _buildsNum,
            hasMore: true,
            builds: props._builds.slice(0, _buildsNum)
        }
    }

    fetchData = () => {
        setTimeout(() => {
            this.setState(state => ({
                _lastItem: state._lastItem + _buildsNum,
                builds: this.props._builds.slice(0, state._lastItem + _buildsNum),
                hasMore: (state._lastItem + _buildsNum <= this.props._builds.length),
            }));

        }, _fetchDelay);
    };

    /*
    * Решение использовать данный подход, в отличие от мемоизации, предложенной документацией
    * обосновано тем, что при таком подходе ввиду асинхронных операций запросов новых данных
    * мы не можем контролировать состояние (состояние может измениться во время перехода к другому состоянию),
    * что в итоге приводит к warning-у:
    * Cannot update during an existing state transition (such as within `render`).
    * Render methods should be a pure function of props and state.
    * */
    static getDerivedStateFromProps(props, state) {
        if (props.orderKey !== state.prevOrderKey || props._builds.length !== state.prevBuilds.length) {
            return {
                prevOrderKey: props.orderKey,
                prevBuilds: props._builds,

                _lastItem: _buildsNum,
                builds: props._builds.slice(0, _buildsNum),
                hasMore: (_buildsNum <= props._builds.length)
            };
        }

        return null;
    }

    handleOrderKeyChange = (key) => {
        this.props.onOrderKeyChange(key);
    };

    render() {
        const {classes} = this.props;

        return (
            <Grid container>
                <Grid item xs={12}
                    className={classes.listHeader}
                >
                    <BuildsListHeader
                        totalBuildsNum={this.props._builds.length}
                        orderKey={this.props.orderKey}
                        onOrderKeyChange={this.handleOrderKeyChange}
                    />
                </Grid>

                <InfiniteScroll
                    dataLength={this.state.builds.length}
                    next={this.fetchData}
                    hasMore={this.state.hasMore}
                    loader={<LinearProgress
                                color={"primary"}
                                className={classes.progress}
                            />}
                    className={classes.scroll}
                >
                    {this.state.builds.map(build => (
                        <Grid key={build.id} item xs={12} className={classes.card}>
                            <BuildCard
                                buildData={build}
                            />
                        </Grid>
                    ))}
                </InfiniteScroll>
            </Grid>
        );
    }
}

export default withStyles(styles)(BuildsList);