import React from 'react';

import {withStyles} from '@material-ui/styles';
import {SvgIcon, Typography, Grid} from '@material-ui/core';

const styles = theme => ({
    root: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2)
    },

    infoWrapper: {
        display: "flex",
        alignItems: "center",
    },

    icon: {
        fontSize: "190%",
        marginRight: theme.spacing(0.5)
    }
});

function BuildInfo(props) {
    const {classes} = props;

    const buildInfo = props.info;

    return (
        <Grid container spacing={1} className={classes.root} justify={"space-evenly"}>
            <Grid item>
                <div className={classes.infoWrapper}>
                    {/* build version */}
                    <SvgIcon className={classes.icon}>
                        <path d="M4,4H20A2,2 0 0,1 22,6V18A2,2 0 0,1 20,20H4A2,2 0 0,1 2,18V6A2,2 0 0,1 4,4M4,6V18H11V6H4M20,18V6H18.76C19,6.54 18.95,7.07 18.95,7.13C18.88,7.8 18.41,8.5 18.24,8.75L15.91,11.3L19.23,11.28L19.24,12.5L14.04,12.47L14,11.47C14,11.47 17.05,8.24 17.2,7.95C17.34,7.67 17.91,6 16.5,6C15.27,6.05 15.41,7.3 15.41,7.3L13.87,7.31C13.87,7.31 13.88,6.65 14.25,6H13V18H15.58L15.57,17.14L16.54,17.13C16.54,17.13 17.45,16.97 17.46,16.08C17.5,15.08 16.65,15.08 16.5,15.08C16.37,15.08 15.43,15.13 15.43,15.95H13.91C13.91,15.95 13.95,13.89 16.5,13.89C19.1,13.89 18.96,15.91 18.96,15.91C18.96,15.91 19,17.16 17.85,17.63L18.37,18H20M8.92,16H7.42V10.2L5.62,10.76V9.53L8.76,8.41H8.92V16Z" />
                    </SvgIcon>

                    <Typography variant={"subtitle1"}>
                        {buildInfo.version}
                    </Typography>
                </div>
            </Grid>

            <Grid item>
                <div className={classes.infoWrapper}>
                    {/* build date */}
                    <SvgIcon className={classes.icon}>
                        <path d="M12,20A8,8 0 0,0 20,12A8,8 0 0,0 12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20M12,2A10,10 0 0,1 22,12A10,10 0 0,1 12,22C6.47,22 2,17.5 2,12A10,10 0 0,1 12,2M12.5,7V12.25L17,14.92L16.25,16.15L11,13V7H12.5Z" />
                    </SvgIcon>

                    <Typography variant={"subtitle1"}>
                        {buildInfo.buildDate.toLocaleString("ru-RU")}
                    </Typography>
                </div>
            </Grid>

            <Grid item>
                <div className={classes.infoWrapper}>
                    {/* bugs num */}
                    <SvgIcon className={classes.icon}>
                        <path d="M20,8H17.19C16.74,7.2 16.12,6.5 15.37,6L17,4.41L15.59,3L13.42,5.17C12.96,5.06 12.5,5 12,5C11.5,5 11.05,5.06 10.59,5.17L8.41,3L7,4.41L8.62,6C7.87,6.5 7.26,7.21 6.81,8H4V10H6.09C6.03,10.33 6,10.66 6,11V12H4V14H6V15C6,15.34 6.03,15.67 6.09,16H4V18H6.81C8.47,20.87 12.14,21.84 15,20.18C15.91,19.66 16.67,18.9 17.19,18H20V16H17.91C17.97,15.67 18,15.34 18,15V14H20V12H18V11C18,10.66 17.97,10.33 17.91,10H20V8M16,15A4,4 0 0,1 12,19A4,4 0 0,1 8,15V11A4,4 0 0,1 12,7A4,4 0 0,1 16,11V15M14,10V12H10V10H14M10,14H14V16H10V14Z" />
                    </SvgIcon>

                    <Typography variant={"subtitle1"}>
                        {buildInfo.bugsNum}
                    </Typography>
                </div>
            </Grid>

            <Grid item>
                <div className={classes.infoWrapper}>
                    {/* file size */}
                    <SvgIcon className={classes.icon}>
                        <path d="M14,2H6A2,2 0 0,0 4,4V20A2,2 0 0,0 6,22H18A2,2 0 0,0 20,20V8L14,2M18,20H6V4H13V9H18V20Z" />
                    </SvgIcon>

                    <Typography variant={"subtitle1"}>
                        {buildInfo.fileSize}
                    </Typography>
                </div>
            </Grid>
        </Grid>
    );
}

export default withStyles(styles)(BuildInfo);