import React from 'react';

import {withStyles} from '@material-ui/styles';

import {Grid, MenuItem, OutlinedInput, Select, SvgIcon, Typography} from "@material-ui/core";
import {textCaption} from "../../../theme";

const styles = theme => ({
    label: {
        ...textCaption,

        // on mobile devices
        [theme.breakpoints.down('md')]: {
            textAlign: 'center'
        }
    },

    svgIcon: {
        marginRight: theme.spacing(1),
        fontSize: 20
    },

    flexContainer: {
        display: "flex",
        alignItems: "center"
    }
});

function BuildsListHeader(props) {
    const handleChange = (event) => {
        props.onOrderKeyChange(event.target.value);
    };

    const {classes} = props;

    return (
        <Grid container
              alignItems={"center"}
              justify={"space-between"}
              spacing={2}
              className={classes.root}
        >
            <Grid item xs={12} sm={3}>
                <Typography variant={"subtitle2"} align={"left"} className={classes.label}>
                    {props.totalBuildsNum + ' builds'}
                </Typography>
            </Grid>

            <Grid item xs={12} sm={9}>
                <div>
                    <Grid
                        container
                        justify={"flex-end"}
                        alignItems={"center"}
                        spacing={1}
                    >
                        <Grid item xs={4} sm={3}>
                            <Typography variant={"subtitle2"} align={"right"} className={classes.label}>
                                Sort by
                            </Typography>
                        </Grid>

                        <Grid item xs={8} sm={4}>
                            <Select
                                value={props.orderKey}
                                onChange={handleChange}
                                input={<OutlinedInput labelWidth={0} name={"orderKey"} id={"list-sorter"}/>}
                                fullWidth
                            >
                                <MenuItem value={"buildDate_asc"}>
                                    <div className={classes.flexContainer}>
                                        <SvgIcon className={classes.svgIcon}>
                                            <path d="M10,11V13H18V11H10M10,5V7H14V5H10M10,17V19H22V17H10M6,7H8.5L5,3.5L1.5,7H4V20H6V7Z" />
                                        </SvgIcon>

                                        <Typography variant={"button"}>
                                            Build date
                                        </Typography>
                                    </div>
                                </MenuItem>

                                <MenuItem value={"buildDate_desc"}>
                                    <div className={classes.flexContainer}>
                                        <SvgIcon className={classes.svgIcon}>
                                            <path d="M10,13V11H18V13H10M10,19V17H14V19H10M10,7V5H22V7H10M6,17H8.5L5,20.5L1.5,17H4V4H6V17Z" />
                                        </SvgIcon>

                                        <Typography variant={"button"}>
                                            Build date
                                        </Typography>
                                    </div>
                                </MenuItem>
                            </Select>
                        </Grid>
                    </Grid>
                </div>
            </Grid>
        </Grid>
    );
}

export default withStyles(styles)(BuildsListHeader);