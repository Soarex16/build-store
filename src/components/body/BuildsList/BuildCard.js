import React from 'react';

import {withStyles} from '@material-ui/styles';
import {Fab, Divider, Button, Card, CardMedia, CardContent, Typography, CardActions} from '@material-ui/core';

import {ArrowBack, ArrowForward} from '@material-ui/icons';

import {Carousel} from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";

import BuildInfo from './BuildInfo';

const styles = theme => ({
    root: {
        borderRadius: 8,
    },

    media: {
        height: theme.spacing(40),
    },

    controls: {
        justifyContent: "flex-end",
        marginBottom: theme.spacing(1),
        marginRight: theme.spacing(1)
    },

    carouselWrapper: {
        position: "relative"
    },

    buttonContainer: {
        padding: theme.spacing(2),
        width: "100%",
        position: "absolute",
        top: "40%",
        zIndex: 1,
    }
});

class BuildCard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            elevation: 1,
            currentSlide: 0
        };
    }

    handleMouseEnter = () => this.setState({ elevation: 8 });

    handleMouseLeave = () => this.setState({ elevation: 1 });

    prevSlide = () => {
        this.setState(state => ({
            currentSlide: state.currentSlide - 1
        }));
    };

    nextSlide = () => {
        this.setState(state => ({
            currentSlide: state.currentSlide + 1
        }));
    };

    handleSlideChange = (index) => {
        if (index !== this.state.currentSlide) {
            this.setState({
                currentSlide: index
            });
        }
    };

    // TODO: try to reimplement carousel with https://codesandbox.io/s/ert96 and https://codesandbox.io/s/jx6qh
    render() {
        const {classes} = this.props;

        const buildData = this.props.buildData;

        const images = [];

        const imgUrls = buildData.carousel.images;
        for (let i = 0; i < imgUrls.length; ++i) {
            images.push(
                <CardMedia
                    key={buildData.id + i}
                    component="img"
                    image={imgUrls[i]}
                    className={classes.media}
                />
            );
        }

        return (
            <Card
                className={classes.root}
                elevation={this.state.elevation}

                onMouseEnter={this.handleMouseEnter}
                onMouseLeave={this.handleMouseLeave}
            >

                <div className={classes.carouselWrapper}>
                    <div className={classes.buttonContainer}>
                        <Fab
                            color="primary"
                            onClick={this.prevSlide}
                            style={{float: 'left'}}
                        >
                            <ArrowBack />
                        </Fab>

                        <Fab
                            color="primary"
                            onClick={this.nextSlide}
                            style={{float: 'right'}}
                        >
                            <ArrowForward />
                        </Fab>
                    </div>

                    <Carousel
                        showArrows={false}
                        showThumbs={false}
                        showStatus={false}
                        infiniteLoop={true}
                        selectedItem={this.state.currentSlide}
                        onChange={this.handleSlideChange}
                    >
                        {images}
                    </Carousel>
                </div>

                <CardContent>
                    <Typography component="h5" variant="h5" gutterBottom>
                        {buildData.product.title}
                    </Typography>

                    <Typography variant="subtitle1" color="textSecondary">
                        {buildData.product.description}
                    </Typography>
                </CardContent>

                <CardActions className={classes.controls}>
                    <Button
                        color={"secondary"}
                        target={"_self"}
                        href={"#"}
                    >
                        Read more
                    </Button>

                    <Button
                        variant={"contained"}
                        color={"primary"}
                        target={"_blank"}
                        href={buildData.build.downloadLink}
                    >
                        Download
                    </Button>
                </CardActions>

                <Divider component={"div"} variant={"middle"}/>

                <BuildInfo info={buildData.build}/>
            </Card>
        );
    }
}

export default withStyles(styles)(BuildCard);