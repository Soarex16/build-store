import React from 'react';

import {withStyles} from '@material-ui/styles';
import {Container, Grid} from '@material-ui/core';

import PlatformSelector from './PlatformSelector/PlatformSelector';
import BuildsList from './BuildsList/BuildsList';

import buildFile from '../../testData';

const styles = theme => ({
    root: {
        marginBottom: theme.spacing(8),
    },
});

const comparators = {
    buildDate_asc: (first, second) => (first.build.buildDate - second.build.buildDate),
    buildDate_desc: (first, second) => (second.build.buildDate - first.build.buildDate)
};

/**
 * Below is a general scheme of the functioning of the Content component.
 *
 * Content
 * |
 * |- PlatformSelector
 * |  |
 * |  |- PlatformSelectorButton_1
 * |  |...
 * |  |- PlatformSelectorButton_n
 * |
 * |- BuildsList
 *    | NOTE: move cards into separate container
 *    |- ListHeader
 *    |- Card_1
 *    |...
 *    |- Card_n
 *
 */

/**
 * Итак, почему мы храним так много информации в омпоненте Content, а не рвзносим ее по дочерним компонентам?
 *
 * В соответствии с документацией реакта, единственный способ передачи данных дочерним компонентам - свойство props,
 * которое является неизменяемым (если, например, мы вынесем сортировку элементов в дочерний BuildsList, то тогда
 * придется копировать весь массив, а потом уже его сортировать, увеличивает количество вычислений и соответственно
 * замедляет загрузку страницы)
 *
 * Поэтому по рекомендации документации данный компонент представляет собой "ближайшего общего предка, который
 * должен хранить в себе состояние дочерних компонентов"
 *
 * UPDATE: теперь данный компонент также выполняет роль стаба и имитирует сервер с информацией
 */

class Content extends React.Component {
    constructor(props) {
        super(props);

        // for converting date strings to date objects
        for(let i = 0; i < buildFile.length; ++i) {
            buildFile[i].build.buildDate = new Date(buildFile[i].build.buildDate);
        }

        this.state = {
            builds: buildFile,

            orderKey: "buildDate_asc",

            osFilter: {
                windows: true,
                android: true,
                ios: true
            },
        };
    }

    // Data transformation methods

    // Будет лучше, если функции сделать чистыми и вынести за пределы компонента
    getFilteredBuilds = () => {
        const osFilter = this.state.osFilter;

        return this.state.builds.filter(item => {
            return osFilter[item.build.targetPlatform];
        });
    };

    orderByComparer = (array) => {
        return array.sort(comparators[this.state.orderKey]);
    };

    // Event handlers

    handleOrderKeyChange = (newOrderKey) => {
        this.setState({
            orderKey: newOrderKey
        });
    };

    handleOsListChange = (osName) => {
        let stateFunc = state => {
            let newFilter = state.osFilter;

            // NOTE: can cause bugs after minification
            newFilter[osName] = !state.osFilter[osName];

            return {
                osFilter: newFilter
            };
        };

        this.setState(stateFunc);
    };

    render() {
        const {classes} = this.props;

        let buildsList = this.getFilteredBuilds();
        buildsList = this.orderByComparer(buildsList);

        return (
            <Container maxWidth={"lg"} className={classes.root}>
                <Grid container spacing={3} alignItems={"baseline"}>
                    <Grid item xs={12} sm={2}>
                        <PlatformSelector
                            osFilter={this.state.osFilter}
                            onOsListChange={this.handleOsListChange}
                        />
                    </Grid>

                    <Grid item xs={12} sm={10}>
                        <BuildsList
                            // Builds only for stub
                            _builds={buildsList}
                            orderKey={this.state.orderKey}
                            onOrderKeyChange={this.handleOrderKeyChange}
                        />
                    </Grid>
                </Grid>
            </Container>
        );
    }
}

export default withStyles(styles)(Content);