import React from 'react';

import {withStyles} from '@material-ui/styles';
import {Typography} from '@material-ui/core';
import SocialNetworks from "./SocialNetworks";

const styles = theme => ({
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6),
    }
});

function Footer(props) {
    const {classes} = props;

    const socialLinks = {
        youtube: "https://www.youtube.com/user/Crocincor",
        twitter: "https://twitter.com/crocinc",
        instagram: "https://www.instagram.com/crocinc/",
        facebook: "https://www.facebook.com/CrocIncorporated"
    };

    return (
        <footer className={classes.footer}>
            <Typography variant="h6" align="center" gutterBottom>
                CROC VR builds store
            </Typography>

            <Typography variant="subtitle1" align="center" component="p">
                Copyright CROC 2009–2019, All Rights Reserved
            </Typography>

            <SocialNetworks links={socialLinks}/>
        </footer>
    );
}

export default withStyles(styles)(Footer);