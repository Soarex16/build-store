import React from 'react';

import {AppBar, Toolbar, Typography, withStyles} from '@material-ui/core';

import HeroPanel from './HeroPanel';

const styles = theme => ({
    heroPanelWrapper: {
        display: "flex",
        marginBottom: theme.spacing(6)
    }
});

function Header(props) {
    const {classes} = props;

    return (
        <React.Fragment>
            <AppBar position={"relative"}>
                <Toolbar>
                    <Typography variant="h6" color="inherit" noWrap>
                        CROC VR store
                    </Typography>
                </Toolbar>
            </AppBar>

            <div className={classes.heroPanelWrapper}>
                <HeroPanel/>
            </div>
        </React.Fragment>
    )
}

export default withStyles(styles)(Header);