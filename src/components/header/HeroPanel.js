import React from 'react'

import {withStyles} from '@material-ui/styles';
import {Container, Grid, Paper, Typography} from '@material-ui/core';

import MainMenu from './MainMenu';

const styles = theme => ({
    root: {
        position: 'relative',

        background: 'linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.45)), url(/heroBackground.jpg)',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',

        width: "100%",
    },

    heroText: {
        margin: theme.spacing(4),
        color: theme.palette.primary.contrastText,
    },

    menuContainer: {
        // На данный момент нет необходимости в меню
        visibility: 'hidden',
        float: "right",
        marginBottom: theme.spacing(10),
    }
});

function HeroPanel(props) {
    const {classes} = props;

    return (
        <React.Fragment>
            <Paper square elevation={12} className={classes.root}>
                <Container maxWidth={"xl"}>
                    <div className={classes.menuContainer}>
                        <MainMenu />
                    </div>

                    <div className={classes.heroText}>
                        <Grid container direction="row" alignItems="baseline">
                            <div>
                                <Container maxWidth="md">
                                    <Typography component="h2" variant="h3" color={"inherit"} paragraph>
                                        Магазин сборок CI/CD
                                    </Typography>

                                    <Typography variant="h5" color="inherit" paragraph>
                                        Commit, build, test and quickly deliver to user your products with
                                        CROC VR continuous integration store
                                    </Typography>
                                </Container>
                            </div>
                        </Grid>
                    </div>
                </Container>
            </Paper>
        </React.Fragment>
    );
}

export default withStyles(styles)(HeroPanel);