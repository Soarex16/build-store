import React from 'react'

import {withStyles} from '@material-ui/styles';
import {Button, Grid, Paper} from '@material-ui/core';

const styles = theme => ({
    root: {
        padding: theme.spacing(2),
    },
    button: {
        width: "100%",
    }
});

function MainMenu(props) {
    const {classes} = props;

    return (
        <Paper className={classes.root}>
            <Grid container spacing={1} justify={"space-evenly"}>

                <Grid item xs={12} sm={6} md={3}>
                    <Button href="#" className={classes.button}>
                        Menu item 1
                    </Button>
                </Grid>

                <Grid item xs={12} sm={6} md={3}>
                    <Button href="#" className={classes.button}>
                        Menu item 2
                    </Button>
                </Grid>

                <Grid item xs={12} sm={6} md={3}>
                    <Button href="#" className={classes.button}>
                        Menu item 3
                    </Button>
                </Grid>

                <Grid item xs={12} sm={6} md={3}>
                    <Button href="#" className={classes.button}>
                        Menu item 4
                    </Button>
                </Grid>
            </Grid>
        </Paper>
    );
}

export default withStyles(styles)(MainMenu);